
import java.util.Arrays;

class SortArray {

	public static void main(String args[]) {

		int initialarray[] = { 0, -3, -3, 20, 10, 20, 20, 9, 9, 5, 1, 1, 1, 3, 5, 6, 7, 8 };
		// int sortedArray[] = new int[initialarray.length]
		int sortedArray[] = sortArray(initialarray, initialarray.length);
		System.out.println(Arrays.toString(sortedArray));

		// To get the third lowest by removing duplicates
		int thirdLowest = getLowest(sortedArray, sortedArray.length, 3);
		System.out.println("Third lowest : " + thirdLowest);
	}

	// Sort array in ascending order
	public static int[] sortArray(int array[], int size) {
		int temp;
		for (int pass = 1; pass < size; pass++) {
			for (int i = 0; i < size - pass; i++) {
				if (array[i] > array[i + 1]) {
					temp = array[i];
					array[i] = array[i + 1];
					array[i + 1] = temp;
				}
			}
		}
		return array;
	}

	// get the position th lowest from the array by removing duplicates
	public static int getLowest(int array[], int size, int position) {
		int count = 0;
		int newArr[] = new int[size];
		// removing duplicates
		for (int a = 0; a < size - 1; a++) {
			if (array[a] != array[a + 1]) {
				newArr[count] = array[a];
				newArr[count + 1] = array[a + 1];
				count = count + 1;
			}
		}
		System.out.println(Arrays.toString(newArr));
		System.out.println("Second Highest : " + newArr[count - 1]);
		return newArr[position - 1];
	}
}