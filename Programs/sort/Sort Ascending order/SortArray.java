import java.util.Arrays;

class SortArray {

	public static void main(String args[]) {

		int array[] = { 9, 9, 9, 1, 3, 5, 6, 7, 8 };
		int temp = 0;

		for (int i = 1; i < array.length; i++) {

			for (int j = 0; j < (array.length - i); j++) {
				try {
					if (array[j] > array[j + 1]) {
						temp = array[j];
						array[j] = array[j + 1];
						array[j + 1] = temp;
					}
				} catch (Exception e) {

				}

			}

		}
		System.out.println(Arrays.toString(array));
	}
}